{
    "id": "506e7082-99f3-409a-b71c-291d5021d8cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_ImagineLoader",
    "eventList": [
        {
            "id": "229202bd-f725-490b-bfe9-a1e6c525d0f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "506e7082-99f3-409a-b71c-291d5021d8cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24cce49e-9eac-4a0b-90dd-ded81207d99a",
    "visible": true
}