{
    "id": "922f6cb6-1abd-430f-9387-0e6903fb1608",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_MatureLoader",
    "eventList": [
        {
            "id": "03ac7176-3dc2-48ea-b0e2-292ebdaa7a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "922f6cb6-1abd-430f-9387-0e6903fb1608"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1af74d66-b353-4e27-8a17-8d87e6468432",
    "visible": true
}