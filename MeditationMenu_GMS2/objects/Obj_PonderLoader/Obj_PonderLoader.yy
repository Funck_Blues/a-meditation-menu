{
    "id": "6e38fde8-b9f5-4c0a-a9b5-a946a5717708",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_PonderLoader",
    "eventList": [
        {
            "id": "ead4add8-5cb2-4f46-a620-6c8949c3ebc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "6e38fde8-b9f5-4c0a-a9b5-a946a5717708"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "240f5a61-3f96-449a-b65c-7450e97012e5",
    "visible": true
}