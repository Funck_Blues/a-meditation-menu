{
    "id": "da744738-61ee-4dd7-ac09-249c2185ee17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_MainLoader",
    "eventList": [
        {
            "id": "a2e2cde2-1224-4013-b539-f80f55b46999",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "da744738-61ee-4dd7-ac09-249c2185ee17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e39055b7-4352-41e5-9868-29276c022349",
    "visible": true
}