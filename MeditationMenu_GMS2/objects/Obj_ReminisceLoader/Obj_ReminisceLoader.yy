{
    "id": "aef385e5-f5fe-453c-9fd7-7d96fe5b0264",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_ReminisceLoader",
    "eventList": [
        {
            "id": "2ff47d5f-dbd3-41c8-a9c7-1cead974c43c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "aef385e5-f5fe-453c-9fd7-7d96fe5b0264"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "319edd2c-56ae-44d5-bf3d-08ce7a0da47a",
    "visible": true
}