{
    "id": "836b0c5f-939a-4c0f-8c35-d01f073fe56e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_EmpathizeLoader",
    "eventList": [
        {
            "id": "dfd40836-bc9d-499a-b5d6-1bf6ec3b0bb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "836b0c5f-939a-4c0f-8c35-d01f073fe56e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ce34527-1b32-45aa-809c-d2395c1aaf5e",
    "visible": true
}