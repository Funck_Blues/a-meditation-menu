{
    "id": "2793ba90-1b1a-4909-b5c3-9b7e67f368dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_ReflectLoader",
    "eventList": [
        {
            "id": "7ebf81bf-4729-410c-a8f1-58f34b632f52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "2793ba90-1b1a-4909-b5c3-9b7e67f368dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e65158be-c8a2-4b7b-9f19-01b1f959bb6f",
    "visible": true
}