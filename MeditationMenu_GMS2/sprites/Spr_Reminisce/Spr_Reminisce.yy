{
    "id": "319edd2c-56ae-44d5-bf3d-08ce7a0da47a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Reminisce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 3,
    "bbox_right": 61,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc2c9f7c-4eed-4c9d-a632-6f95bfe95337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "319edd2c-56ae-44d5-bf3d-08ce7a0da47a",
            "compositeImage": {
                "id": "b52bc011-b3a3-445b-99b2-f50ba23e0bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2c9f7c-4eed-4c9d-a632-6f95bfe95337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad2bbde3-b8bf-4c88-b01b-5f11f2e3c9be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2c9f7c-4eed-4c9d-a632-6f95bfe95337",
                    "LayerId": "8be99e7b-4acc-47e4-8489-150aba5eda54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8be99e7b-4acc-47e4-8489-150aba5eda54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "319edd2c-56ae-44d5-bf3d-08ce7a0da47a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}