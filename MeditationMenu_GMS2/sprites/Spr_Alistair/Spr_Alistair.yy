{
    "id": "063180de-b6fe-4373-ae9c-2fc7d7a9d990",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Alistair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 20,
    "bbox_right": 48,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4766ec9b-e007-4a13-9d75-fd1b7321c582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "063180de-b6fe-4373-ae9c-2fc7d7a9d990",
            "compositeImage": {
                "id": "7ea4ccec-abee-4e41-af6a-fd158b128c44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4766ec9b-e007-4a13-9d75-fd1b7321c582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6e57cf8-fa93-4021-8630-d404526b152d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4766ec9b-e007-4a13-9d75-fd1b7321c582",
                    "LayerId": "0e080dcb-97db-46b3-9bf9-e7ee5d663182"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0e080dcb-97db-46b3-9bf9-e7ee5d663182",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "063180de-b6fe-4373-ae9c-2fc7d7a9d990",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}