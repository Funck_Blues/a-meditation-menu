{
    "id": "24cce49e-9eac-4a0b-90dd-ded81207d99a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Imagine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73a2f602-9ace-4043-861b-971ce7e28439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24cce49e-9eac-4a0b-90dd-ded81207d99a",
            "compositeImage": {
                "id": "b40060a7-2b4c-4d10-b109-82d1a6de4251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a2f602-9ace-4043-861b-971ce7e28439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35eda26a-89ec-4e68-8154-446c01f12115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a2f602-9ace-4043-861b-971ce7e28439",
                    "LayerId": "fa8635bc-95c8-40e0-90d0-832cfaa592d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fa8635bc-95c8-40e0-90d0-832cfaa592d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24cce49e-9eac-4a0b-90dd-ded81207d99a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}