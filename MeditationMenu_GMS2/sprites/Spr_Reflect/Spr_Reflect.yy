{
    "id": "e65158be-c8a2-4b7b-9f19-01b1f959bb6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Reflect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 12,
    "bbox_right": 50,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82f53edd-115a-4fe3-8e0e-f66fc20d5382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e65158be-c8a2-4b7b-9f19-01b1f959bb6f",
            "compositeImage": {
                "id": "45037572-a2a3-47f8-9e5f-b085f9f69dad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f53edd-115a-4fe3-8e0e-f66fc20d5382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46531925-4619-49ba-b688-bccaa9e26659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f53edd-115a-4fe3-8e0e-f66fc20d5382",
                    "LayerId": "57ead39a-5162-4283-be02-ba95614cd84a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "57ead39a-5162-4283-be02-ba95614cd84a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e65158be-c8a2-4b7b-9f19-01b1f959bb6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}