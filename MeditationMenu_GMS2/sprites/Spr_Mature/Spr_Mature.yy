{
    "id": "1af74d66-b353-4e27-8a17-8d87e6468432",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Mature",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 11,
    "bbox_right": 53,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75b50a29-4257-422f-9ae7-225612fc1748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af74d66-b353-4e27-8a17-8d87e6468432",
            "compositeImage": {
                "id": "3f066440-2561-4476-88bc-a63225f5aaff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b50a29-4257-422f-9ae7-225612fc1748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ee8277-c8c4-4c5f-8404-024e35748db0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b50a29-4257-422f-9ae7-225612fc1748",
                    "LayerId": "f202641c-525c-414e-9ad9-725ec6d8acdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f202641c-525c-414e-9ad9-725ec6d8acdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1af74d66-b353-4e27-8a17-8d87e6468432",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}