{
    "id": "8ce34527-1b32-45aa-809c-d2395c1aaf5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Empathize",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8128373b-573e-4b0a-8880-f0e2865bd013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ce34527-1b32-45aa-809c-d2395c1aaf5e",
            "compositeImage": {
                "id": "94705a08-befc-4d67-8073-17baf0f0901d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8128373b-573e-4b0a-8880-f0e2865bd013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ca9015-0c2b-469d-9457-bb8c808e09dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8128373b-573e-4b0a-8880-f0e2865bd013",
                    "LayerId": "d94dd1df-b802-4e87-93b5-8147d9342ae8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d94dd1df-b802-4e87-93b5-8147d9342ae8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce34527-1b32-45aa-809c-d2395c1aaf5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}