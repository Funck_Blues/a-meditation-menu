{
    "id": "e39055b7-4352-41e5-9868-29276c022349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Main",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 8,
    "bbox_right": 54,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98c1cc5c-7e3d-427a-872b-3dd892093ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e39055b7-4352-41e5-9868-29276c022349",
            "compositeImage": {
                "id": "6a051e37-cf18-42ed-8e77-115aebaac626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c1cc5c-7e3d-427a-872b-3dd892093ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae31902-93b7-43cf-b4bd-f41bc089e5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c1cc5c-7e3d-427a-872b-3dd892093ffe",
                    "LayerId": "06f36867-5572-41f9-9da7-d2da75b221e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "06f36867-5572-41f9-9da7-d2da75b221e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e39055b7-4352-41e5-9868-29276c022349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}