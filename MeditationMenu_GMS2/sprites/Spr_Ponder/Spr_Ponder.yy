{
    "id": "240f5a61-3f96-449a-b65c-7450e97012e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Ponder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c7afdf2-8ce9-4099-a361-0c5ceeffb88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "240f5a61-3f96-449a-b65c-7450e97012e5",
            "compositeImage": {
                "id": "d7d422ce-4b5f-43fb-bb06-45c37df208bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7afdf2-8ce9-4099-a361-0c5ceeffb88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b26d43a-da5b-43f7-a606-53c50b73cb4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7afdf2-8ce9-4099-a361-0c5ceeffb88d",
                    "LayerId": "ff32e850-993c-4d8d-8e5c-fdfac02f4791"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff32e850-993c-4d8d-8e5c-fdfac02f4791",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "240f5a61-3f96-449a-b65c-7450e97012e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}